package com.example.demo.controllers;


import com.example.demo.models.Account;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("account/")
public class AccountApi {

    private List<Account> accounts;

    public AccountApi(){
        this.accounts = new ArrayList<>();

        this.accounts.add(new Account("1","First",1000.0,"1"));
        this.accounts.add(new Account("2","Second", 100.0,"2"));
        this.accounts.add(new Account("3","Third", 100.0,"1"));
        this.accounts.add(new Account("4","Second", 500.0,"3"));

    }

    @GetMapping("/{registrationId}")
    public List<Account> getAccountsById(
            @PathVariable("registrationId") String registrationId) {

        List<Account> collect = accounts.stream()
                .filter(x -> registrationId.equals(x.getRegistrationId()))
                .collect(Collectors.toList());

        return collect;
    }

    @GetMapping("/all")
    public List<Account> getAllAccounts(){
        return accounts;
    }

    @GetMapping("/detail/{id}")
    public Account getBookById(@PathVariable("id") String id) {
        for (Account account : accounts) {
            if (account.getId().equals(id)) {
                return account;
            }
        }
        return null;
    }


}
