package com.example.demo.services;


import com.example.demo.models.AccountRequest;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
public class Consumer {

    @KafkaListener(topics = "account_requests", groupId = "group_id")
    public void consume(AccountRequest accountRequest) throws IOException {
        System.out.println(String.format("#### -> Notify user by email: -> %s",
                "User " + accountRequest.getUserId() + " purchased service,which called "
                        + accountRequest.getAccount().toString()));
    }
}
