package com.example.demo.controllers;

import com.example.demo.modules.PatientRegisInfo;
import com.example.demo.modules.Regis;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("regis/info")
public class RegisInfoController {

    @GetMapping("/{userId}")
    public PatientRegisInfo getDeseasesByUserId(
            @PathVariable("userId") String userId) {

        List<Regis> patientRegisInfo = Arrays.asList(
                new Regis("1","4" ,"1","Big problem with teeth and need to put braces", "Middle", "Active", "Buy special pills"),
                new Regis("2","4","2","Headaches and high temperature in two days", "High", "Healthy", "Relax"),
                new Regis("3","5","2","Headaches","High","Active","Relaxation"));

//        PatientRegisInfo patientRegis = new PatientRegisInfo(patientRegisInfo);

        List<Regis> collect = patientRegisInfo.stream()
                .filter(x -> userId.equals(x.getUserId()))
                .collect(Collectors.toList());

        PatientRegisInfo patientRegis = new PatientRegisInfo(collect);

        return patientRegis;
    }
}
