package com.example.demo.modules;

import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Regis {
    private String id;
    private String userId;
    private String doctorId;
    private String description;
    private String emergencyLevel;
    private String status;
    private String treatment_comments;
}
