package com.example.demo.modules;

import lombok.*;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class PatientRegisInfo {
    private List<Regis> patientRegistration;
}
