package com.example.demo.services;

import com.example.demo.models.AccountRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class Producer {

    private static final String TOPIC = "account_requests";

    @Autowired
    private KafkaTemplate<String, AccountRequest> kafkaTemplate;

    public String AccountRequestNotify(AccountRequest accountRequest) {
        System.out.println(String.format("#### -> Producing account request to notification service -> %s", accountRequest));
        this.kafkaTemplate.send(TOPIC, accountRequest);
        return "Successfully";
    }
}
