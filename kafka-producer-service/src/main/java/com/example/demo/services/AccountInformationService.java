package com.example.demo.services;

import com.example.demo.models.Account;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

@Service
public class AccountInformationService {

    @Autowired
    private RestTemplate restTemplate;


    @HystrixCommand(
            fallbackMethod = "getAccountInformationFallback",
            threadPoolKey = "getAccountInformation",
            threadPoolProperties = {
                    @HystrixProperty(name="coreSize", value="100"),
                    @HystrixProperty(name="maximumSize", value="120"),
                    @HystrixProperty(name="maxQueueSize", value="50"),
                    @HystrixProperty(name="allowMaximumSizeToDivergeFromCoreSize", value="true")
            }
    )
    public Account getAccountInformation(String id){

        String apiCredentials = "rest-client:111";
        String base64Credentials = new String(Base64.encodeBase64(apiCredentials.getBytes()));

        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Basic " + base64Credentials);
        HttpEntity<String> entity = new HttpEntity<>(headers);


        return restTemplate.exchange(
                "http://account-info-service/account/detail/" + id,
                HttpMethod.GET,
                entity, Account.class).getBody();
    }

    public Account getAccountInformationFallback(String id){
        return new Account("-1","Not available",0.0);
    }
}
