package com.example.demo.models;


import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class AccountRequest {
    private String userId;
    private Account account;

    public String toString() {
        return "AccountRequest{" +
                "userId='" + userId + '\'' +
                ", account=" + account +
                '}';
    }
}
