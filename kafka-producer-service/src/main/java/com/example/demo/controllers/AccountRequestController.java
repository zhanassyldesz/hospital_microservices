package com.example.demo.controllers;


import com.example.demo.models.AccountRequest;
import com.example.demo.services.AccountInformationService;
import com.example.demo.services.Producer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/account/request")
public class AccountRequestController {

    private final Producer producer;
    private AccountInformationService accountInformationService;


    @Autowired
    public AccountRequestController(Producer producer, AccountInformationService accountInformationService){
        this.producer = producer;
        this.accountInformationService = accountInformationService;
    }

    @PostMapping
    public String sendMessageToKafkaTopic(@RequestParam("userId") String userId,@RequestParam("accountId") String accountId){
        AccountRequest accountRequest = new AccountRequest(userId,accountInformationService.getAccountInformation(accountId));
        this.producer.AccountRequestNotify(accountRequest);
        return "Your request sent successful!";
    }

}
