package com.example.demo.controller;

import com.example.demo.modules.Doctor;
import com.example.demo.repository.DoctorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;


@RestController
@RequestMapping("/doctor")
public class DoctorApiController {

    @Autowired
    DoctorRepository doctorRepository;

//    private List<Doctor> doctors;
//
//    public DoctorApiController(){
//        this.doctors = new ArrayList<>();
//
//        this.doctors.add(new Doctor("1","1", "Nick","Fury","Cambridge University","4 years","cardiologist"));
//        this.doctors.add(new Doctor("1","2", "Amanda","Fury","Cambridge University","4 years","cardiologist"));
//
//    }

//    @GetMapping("/{registrationId}")
//    public Doctor getDoctorsById(
//            @PathVariable("registrationId") String registrationId) {
//        return this.doctors.stream().filter(aut -> aut.getRegistrationId().equals(registrationId)).findFirst().get();
//    }

    @GetMapping
    public List<Doctor> index(){
        return doctorRepository.findAll();
    }

    @GetMapping("/{registrationId}")
    public Doctor getDoctorsById(@PathVariable("registrationId") String registrationId) {
        int id = Integer.parseInt(registrationId);
        return doctorRepository.findById(id);
    }


    @PostMapping("/createDoctor")
    public Doctor create(@RequestBody Map<String, String> body){
        String name = body.get("name");
        String surname = body.get("surname");
        String education = body.get("education");
        String experience = body.get("experience");
        String position = body.get("position");
        return doctorRepository.save(new Doctor(name, surname, education,experience,position));
    }

}
