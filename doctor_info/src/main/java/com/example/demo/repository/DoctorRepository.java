package com.example.demo.repository;

import com.example.demo.modules.Doctor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository("Doctor")
public interface DoctorRepository extends JpaRepository<Doctor, String> {
    Doctor findById(int id);
}
