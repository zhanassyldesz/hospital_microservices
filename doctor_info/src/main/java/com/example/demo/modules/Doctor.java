package com.example.demo.modules;

import lombok.*;

import javax.persistence.*;


@Data
@Getter
@Setter
@Entity
@Table(name = "doctor")
public class Doctor {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    private String name;
    private String surname;
    private String education;
    private String experience;
    private String position;

    public Doctor() {  }

    public Doctor(String name, String surname, String education, String experience,String position) {
        this.setName(name);
        this.setSurname(surname);
        this.setEducation(education);
        this.setExperience(experience);
        this.setPosition(position);
    }

    public Doctor(int id, String name, String surname, String education, String experience,String position) {
        this.setId(id);
        this.setName(name);
        this.setSurname(surname);
        this.setEducation(education);
        this.setExperience(experience);
        this.setPosition(position);
    }
}
