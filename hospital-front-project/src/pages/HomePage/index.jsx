import * as React from 'react';
import { withStyles, makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import { withRouter } from 'react-router-dom';
import axios from '../../axios';

const StyledTableCell = withStyles(theme => ({
    head: {
        backgroundColor: theme.palette.common.black,
        color: theme.palette.common.white,
    },
    body: {
        fontSize: 14,
    },
}))(TableCell);

const StyledTableRow = withStyles(theme => ({
    root: {
        '&:nth-of-type(odd)': {
            backgroundColor: theme.palette.background.default,
        },
    },
}))(TableRow);

const useStyles = makeStyles({
    root: {
        width: '100%',
        overflowX: 'auto',
        margin: 'auto'
    },
    table: {
        minWidth: 700,
    },
});

const UserCard = ({username, role}) => {
    const classes = makeStyles({
        card: {
            maxWidth: 345,
            marginBottom: 50,
        },
        media: {
            height: 140,
        },
    })();
    return (
        <Card className={classes.card}>
            <CardActionArea>
                <CardMedia
                    className={classes.media}
                    image={'/images/man.svg'}
                    title="Contemplative Reptile"
                />
                <CardContent>
                    <Typography gutterBottom variant="h5" component="h2">
                        { username }
                    </Typography>
                    <Typography variant="h6" color="textSecondary" component="p">
                        Role: {role}
                    </Typography>
                </CardContent>
            </CardActionArea>
        </Card>
    );
};

export const HomePage = (props) => {
    const classes = useStyles();

    const [ list, setList ] = React.useState([]);
    const [ info, setInfo ] = React.useState({username: '', role: ''});

    React.useEffect(() => {
        axios.get('/api/list/' + sessionStorage.getItem('username'))
            .then(r => {
                setList(r.data);
            })
            .catch(console.log)
    }, []);

    React.useEffect(() => {
        axios.get('/sign/user/' + sessionStorage.getItem('username'))
            .then(r => {
                setInfo({
                    username: r.data.username,
                    role: r.data.role,
                })
            })
            .catch(console.log)
    }, []);
    return (
        <Paper className={classes.root}>
            {info.username && info.role && (
                <UserCard
                    username={info.username}
                    role={info.role}
                />
            )}

            <Table className={classes.table} aria-label="customized table">
                <TableHead>
                    <TableRow>
                        <StyledTableCell>Description</StyledTableCell>
                        <StyledTableCell align="right">Status</StyledTableCell>
                        <StyledTableCell align="right">Treatment</StyledTableCell>
                        <StyledTableCell align="right">Doctor Name</StyledTableCell>
                        <StyledTableCell align="right">Doctor Position</StyledTableCell>
                        <StyledTableCell align="right">All Price</StyledTableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {list.map((it, ind) => (
                        <StyledTableRow key={ind}>
                            <StyledTableCell component="th" scope="row">{it.description}</StyledTableCell>
                            <StyledTableCell align="right">{it.status}</StyledTableCell>
                            <StyledTableCell align="right">{it.treatment}</StyledTableCell>
                            <StyledTableCell align="right">{it.doctor_name}</StyledTableCell>
                            <StyledTableCell align="right">{it.doctor_position}</StyledTableCell>
                            <StyledTableCell align="right">{it.allPrice}</StyledTableCell>
                        </StyledTableRow>
                    ))}
                </TableBody>
            </Table>
        </Paper>
    )
};

export default withRouter(HomePage);

