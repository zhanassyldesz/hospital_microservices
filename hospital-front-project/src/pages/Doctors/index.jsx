import * as React from 'react';
import axios from '../../axios';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Modal from '@material-ui/core/Modal';
import Backdrop from '@material-ui/core/Backdrop';
import Fade from '@material-ui/core/Fade';
import { withRouter } from 'react-router-dom';



const useStyles = makeStyles(theme => ({
    card: {
        maxWidth: 345,
        flexBasis: 345
    },
    media: {
        height: 140,
    },
    modal: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    },
    paper: {
        backgroundColor: theme.palette.background.paper,
        border: '2px solid #000',
        boxShadow: theme.shadows[5],
        padding: theme.spacing(2, 4, 3),
        width: '80%'
    },
}));

export const Doctors = (props) => {

    const classes = useStyles();
    const [doctors, setDoctors] = React.useState([]);
    const [open, setOpen] = React.useState(false);
    const [selectedDoctor, setSelectedDoctor] = React.useState(null);
    const [reviews, setReviews] = React.useState([]);

    React.useEffect(() => {
        if (selectedDoctor) {
            axios.get('/api/list/getReviews/' + selectedDoctor)
                .then(r => {
                    setReviews(r.data);
                });
        }
    }, [selectedDoctor]);

    React.useEffect(
        () => {
            axios.get(`/api/list/getDoctors`)
                .then((r) => {
                    setDoctors(r.data)
                });
        },
        []
    );

    const handleOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };

    return (
        <div style={{
            display: 'flex',
            justifyContent: 'space-between'
        }}>
            {doctors.map((doctor, i) => (
                <Card className={classes.card} key={doctor.id}>
                    <CardActionArea>
                        <CardMedia
                            className={classes.media}
                            image={`/images/${i+1}.jpg`}
                            title="Contemplative Reptile"
                        />
                        <CardContent>
                            <Typography gutterBottom variant="h5" component="h2">
                                { doctor.name } {doctor.surname}
                            </Typography>

                            <Typography gutterBottom  component="p">
                                Experience: {doctor.position}, {doctor.experience}
                            </Typography>

                            <Typography variant="body2" color="textSecondary" component="p">
                                Education: { doctor.education }
                            </Typography>
                        </CardContent>
                    </CardActionArea>
                    <CardActions>
                        <Button size="small" color="primary" onClick={() => {
                            setSelectedDoctor(doctor.id);
                            handleOpen();
                        }}>
                            Watch Review
                        </Button>
                    </CardActions>
                </Card>
            ))}

            <Modal
                aria-labelledby="transition-modal-title"
                aria-describedby="transition-modal-description"
                className={classes.modal}
                open={open}
                onClose={handleClose}
                closeAfterTransition
                BackdropComponent={Backdrop}
                BackdropProps={{
                    timeout: 500,
                }}
            >
                <Fade in={open}>
                    <div className={classes.paper}>
                        {reviews && reviews.length && reviews.map(review => (
                            <Card key={review.id} style={{marginBottom: 30}}>
                                <CardContent style={{display: 'flex', justifyContent: 'space-between'}}>
                                    <Typography gutterBottom variant='h5'  component="p" style={{marginRight: 30}}>
                                        {review.comment}
                                    </Typography>

                                    <Typography variant='h5' component="p">
                                        {review.rating}
                                    </Typography>
                                </CardContent>
                            </Card>
                        ))}
                    </div>
                </Fade>
            </Modal>
        </div>
    );
}

export default withRouter(Doctors)
