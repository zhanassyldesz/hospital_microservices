import React from 'react';
import {
    Switch,
    Route,
    useHistory,
    withRouter
} from "react-router-dom"
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import TypoGraphy from '@material-ui/core/Typography'
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import { makeStyles } from '@material-ui/core/styles';
import ListItemText from '@material-ui/core/ListItemText';
import {Doctors} from '../Doctors';
import {HomePage} from '../HomePage';
import {ServiceCart} from '../ServiceCart';


function NavBar(props) {

    const history = useHistory();

    return (
        <List component="nav">
            <ListItem component="div">
                <ListItemText style={{cursor: 'pointer'}} onClick={() => {
                    history.push('/')
                }} inset>
                    <TypoGraphy color="inherit">
                        Home
                    </TypoGraphy>
                </ListItemText>

                <ListItemText style={{cursor: 'pointer'}} onClick={() => {
                    history.push('/service-cart')
                }} inset>
                    <TypoGraphy color="inherit">
                        Services
                    </TypoGraphy>
                </ListItemText>

                <ListItemText style={{cursor: 'pointer'}} onClick={() => {
                    history.push('/doctors')
                }} inset>
                    <TypoGraphy color="inherit">
                        Doctors
                    </TypoGraphy>
                </ListItemText>
            </ListItem >

        </List>
    )
}

const useStyles = makeStyles(theme => ({
    toolbar: {
        paddingTop: theme.spacing(1),
        paddingBottom: theme.spacing(2),
    },
    logoutButton: {
        marginLeft: 'auto'
    }
}));


export const Layout = (props) => {
    const styles = useStyles();
    const history = useHistory();

    React.useEffect(() => {
        if (!sessionStorage.getItem('token')) {
            history.push('/login')
        }
    }, []);

    return (
        <div>
            <AppBar color="primary" position="static">
                <Toolbar className={styles.toolbar}>
                    <TypoGraphy variant="h6"
                                color="inherit"
                    >
                        Hospital Registry
                    </TypoGraphy>
                    <NavBar/>

                    <IconButton
                        onClick={() => {
                            sessionStorage.removeItem('token');
                            history.push('/login');
                        }}
                        aria-label="display more actions"
                        edge="end"
                        color="inherit"
                        className={styles.logoutButton}
                    >
                        <ExitToAppIcon />
                    </IconButton>
                </Toolbar>
            </AppBar>
            <div style={{
                width: '80%',
                margin: 'auto',
                marginTop: 40
            }}>
            <Switch>
                <Route path='/service-cart' component={ServiceCart} />
                <Route path='/doctors' component={Doctors} />
                <Route path='/' component={HomePage} />
            </Switch>
            </div>
        </div>
    );
};

export default withRouter(Layout);

