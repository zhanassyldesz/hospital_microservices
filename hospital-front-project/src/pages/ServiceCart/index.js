import React from 'react';
import MaterialTable from "material-table";
import { withRouter } from 'react-router-dom';
import axios from '../../axios';

export const ServiceCart = (props) => {
    const columns = React.useMemo(() => [
        { title: "Service", field: "description" },
        { title: "Price", field: "price" },
    ], []);
    const [services, setServices] = React.useState([]);

    React.useEffect(() => {
        axios.get('/api/list/getServices')
            .then(r => {
                setServices(r.data);
            })
            .catch(console.error.bind(null, 'Error: '))
    }, []);

    const onServiceSelected = (id) => {
        axios
            .post(`/account/request?userId=1&accountId=${id}`)
            .then(console.log)
            .catch(console.error)
    };

    return (
        <MaterialTable
            title="Service List"
            columns={columns}
            data={services}
            actions={[
                {
                    icon: "save",
                    tooltip: "Save User",
                    onClick: (event, rowData) => {
                        onServiceSelected(rowData.id)
                    }
                }
            ]}
        />
    )
};

export default withRouter(ServiceCart);
