import axios from 'axios';

export default axios.create({
    baseURL: 'http://localhost:8762',
    headers: {
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Expose-Headers' : '*',
        ...(sessionStorage.getItem('token') ? {
            'Authorization': sessionStorage.getItem('token')
        } : {})
    }
})
