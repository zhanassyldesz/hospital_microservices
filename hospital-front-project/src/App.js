import React from 'react';
import { Route, Router, BrowserRouter  } from 'react-router-dom';
import { createBrowserHistory } from 'history'
import { Login } from './pages/Login';
import { RegisterPage } from './pages/Register';
import {Layout} from './pages/Layout';
import './App.css';

const history = createBrowserHistory();

const routes = (props) => (
  <BrowserRouter>
        <Route path={'/register'} component={RegisterPage} />
        <Route path='/login' component={Login} />
        <Route path='/' component={Layout}/>
  </BrowserRouter>
);

export default routes;
