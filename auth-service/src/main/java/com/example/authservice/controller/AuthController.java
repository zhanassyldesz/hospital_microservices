package com.example.authservice.controller;


import com.example.authservice.JwtConfig;
import com.example.authservice.model.AppUser;
import com.example.authservice.repository.UserRepository;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import org.apache.tomcat.util.net.openssl.ciphers.Authentication;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping("/sign")
public class AuthController {
    @Autowired
    private UserRepository userRepository;

    private JwtConfig jwt;

    @Autowired
    private BCryptPasswordEncoder encoder;

    @PostMapping
    public AppUser createUser(@RequestBody AppUser appUser) {
        AppUser user = appUser;
        user.setPassword(encoder.encode(appUser.getPassword()));
        user.setRole("USER");
        return userRepository.save(user);
    }

    @GetMapping
    public List<AppUser> getUsers() {
        List<AppUser> users = userRepository.findAll();
        return users;
    }

    @GetMapping("/user/{username}")
    public AppUser currentUserName(@PathVariable(value="username") String username) {
        AppUser user = userRepository.findByUsername(username);
        return user;
    }

//    @GetMapping("/getinfo")
//    public String getSomething(@RequestHeader("Authorization") String token){
//
//        String a = token;
//        String b = a.split(" ")[1];
//        System.out.println(Jwts.parser());
//        Claims claims = Jwts.parser()
//                .setSigningKey(jwt.getSecret().getBytes())
//                .parseClaimsJws(b)
//                .getBody();
//
//        String username = claims.getSubject();
//        return username;
//    }
}
