package com.example.demo.controllers;

import com.example.demo.models.DoctorComments;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("doctors/comments/")
public class DoctorCommentsController {

    private List<DoctorComments> comments;

    public DoctorCommentsController(){
        this.comments = new ArrayList<>();

        this.comments.add(new DoctorComments("1","This is good doctor",5.5,"1","4"));
        this.comments.add(new DoctorComments("2","Bad comment",3.3,"1","5"));
        this.comments.add(new DoctorComments("3","Middle",5.2,"2","4"));
        this.comments.add(new DoctorComments("4","That's middle too",5.6,"3","4"));


    }

    @GetMapping("/{doctorId}")
    public List<DoctorComments> getCommentsbyDoctorId(@PathVariable("doctorId") String doctorId){
        List<DoctorComments> collect = comments.stream()
                .filter(x -> doctorId.equals(x.getDoctorId()))
                .collect(Collectors.toList());

        return collect;
    }

    @GetMapping("/allComments")
    public List<DoctorComments>getComments(){
        return comments;
    }
}
