package microservices.demo.controllers;

import microservices.demo.modules.*;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import microservices.demo.services.AccountantInfoService;
import microservices.demo.services.DoctorCommentsService;
import microservices.demo.services.DoctorInfoService;
import microservices.demo.services.PatientRegistrationInformationService;
import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("api/list")
public class RegistrationListApi {

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private PatientRegistrationInformationService patientRegistrationInformationService;

    @Autowired
    private DoctorInfoService doctorInfoService;

    @Autowired
    private AccountantInfoService accountantInfoService;

    @Autowired
    private DoctorCommentsService doctorCommentsService;

    @GetMapping("/{username}")
    public List<RegistrationList> getAllRegistrations(@PathVariable String username) {

        User user = restTemplate.getForObject("http://auth-service/sign/user/" + username, User.class);
        System.out.println(user);
        String userId = user.getId().toString();
        System.out.println(userId);

        PatientRegisInfo patientInfo = patientRegistrationInformationService.getPatientRegisInformation(userId);

        List<RegistrationList> RegisList = new ArrayList<>();

        for (Regis reg : patientInfo.getPatientRegistration()) {

                Doctor doctor = doctorInfoService.getDoctors(reg.getDoctorId());

//                ResponseEntity<List<Account>> response = accountantInfoService.getAccountInformation(reg.getId());

            List<Account> employees = accountantInfoService.getAccountInformation(reg.getId());
            List<String> des = employees.stream().map(Account::getDescription).sequential().collect(Collectors.toList());

//            Double sum = employees.stream().reduce(0, (a, b) -> a.getPrice() + b.getPrice())
            Double acc = 0.0;

            for (Account a: employees) {
                acc += a.getPrice();
            }


            RegisList.add(new RegistrationList(reg.getDescription(),
                    reg.getStatus(), reg.getTreatment_comments(),doctor.getName(),doctor.getPosition(),acc,des));
        }
        return RegisList;
    }

    @GetMapping("/getServices")
    public List<Account> getAllServices(){

        String apiCredentials = "rest-client:111";
        String base64Credentials = new String(Base64.encodeBase64(apiCredentials.getBytes()));

        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Basic " + base64Credentials);
        HttpEntity<String> entity = new HttpEntity<>(headers);

        return restTemplate.exchange(
                "http://account-info-service/account/all",
                HttpMethod.GET,
                entity,
                new ParameterizedTypeReference<List<Account>>(){}).getBody();

    }

    @GetMapping("/getUsers/{username}")
    public User getUser(@PathVariable(value="username") String username){
        return restTemplate.getForObject("http://auth-service/sign/user/" + username, User.class);
    }

    @GetMapping("/getDoctors")
    public List<Doctor> getAllDoctors(){
        String apiCredentials = "rest-client:321";
        String base64Credentials = new String(Base64.encodeBase64(apiCredentials.getBytes()));

        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Basic " + base64Credentials);
        HttpEntity<String> entity = new HttpEntity<>(headers);

        return restTemplate.exchange(
                "http://doctor-info-service/doctor",
                HttpMethod.GET,
                entity,
                new ParameterizedTypeReference<List<Doctor>>(){}).getBody();
    }

    @GetMapping("/getReviews/{doctorId}")
    public List<DoctorComments> getAllComments(@PathVariable("doctorId") String doctorId){
        List<DoctorComments> comments = doctorCommentsService.getDoctorCommentsInfo(doctorId);
        return comments;
    }
}
