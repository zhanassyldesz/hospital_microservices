package microservices.demo.modules;

import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class DoctorComments {
    private String id;
    private String comment;
    private Double rating;
    private String doctorId;
    private String userId;
}
