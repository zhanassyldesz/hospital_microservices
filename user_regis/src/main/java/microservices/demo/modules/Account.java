package microservices.demo.modules;

import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Account {
    private String id;
    private String description;
    private Double price;
    private String registrationId;
}
