package microservices.demo.modules;

import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Doctor {
    private String id;
    private String name;
    private String surname;
    private String education;
    private String experience;
    private String position;
}
