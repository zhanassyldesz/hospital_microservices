package microservices.demo.modules;

import lombok.*;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class RegistrationList {
    private String description;
    private String status;
    private String treatment;
    private String doctor_name;
    private String doctor_position;
    private Double allPrice;
    private List<String> des;
}
