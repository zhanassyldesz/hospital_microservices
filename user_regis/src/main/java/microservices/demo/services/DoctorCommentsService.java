package microservices.demo.services;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import microservices.demo.modules.DoctorComments;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.apache.commons.codec.binary.Base64;


import java.util.ArrayList;
import java.util.List;

@Service
public class DoctorCommentsService {

    @Autowired
    private RestTemplate restTemplate;


    @HystrixCommand(
            fallbackMethod = "getDoctorCommentsFallback",
            threadPoolKey = "getDoctorCommentsInfo",
            threadPoolProperties = {
                    @HystrixProperty(name="coreSize", value="100"),
                    @HystrixProperty(name="maximumSize", value="120"),
                    @HystrixProperty(name="maxQueueSize", value="50"),
                    @HystrixProperty(name="allowMaximumSizeToDivergeFromCoreSize", value="true")
            }
    )
    public List<DoctorComments> getDoctorCommentsInfo(String doctorId){
        String apiCredentials = "rest-client:555";
        String base64Credentials = new String(Base64.encodeBase64(apiCredentials.getBytes()));

        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Basic " + base64Credentials);
        HttpEntity<String> entity = new HttpEntity<>(headers);

        return restTemplate.exchange("http://doctor-comments-service/doctors/comments/" + doctorId,
                HttpMethod.GET,
                entity,
                new ParameterizedTypeReference<List<DoctorComments>>(){}).getBody();
    }

    public List<DoctorComments> getDoctorCommentsFallback(String doctorId){

        List<DoctorComments> list = new ArrayList<>();
        list.add(new DoctorComments("-1","Not available",0.0,doctorId,"None"));
        return list;
    }

   /* @HystrixCommand(
            fallbackMethod = "getAccountInformationFallback",
            threadPoolKey = "getAccountInformation",
            threadPoolProperties = {
                    @HystrixProperty(name="coreSize", value="100"),
                    @HystrixProperty(name="maximumSize", value="120"),
                    @HystrixProperty(name="maxQueueSize", value="50"),
                    @HystrixProperty(name="allowMaximumSizeToDivergeFromCoreSize", value="true")
            }
    )
    public List<Account> getAccountInformation(String regId){

        String apiCredentials = "rest-client:111";
        String base64Credentials = new String(Base64.encodeBase64(apiCredentials.getBytes()));

        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Basic " + base64Credentials);
        HttpEntity<String> entity = new HttpEntity<>(headers);


        return restTemplate.exchange(
                "http://account-info-service/account/" + regId,
                HttpMethod.GET,
                entity,
                new ParameterizedTypeReference<List<Account>>(){}).getBody();
    }

    public List<Account> getAccountInformationFallback(String regId){

        List<Account> list = new ArrayList<>();
        list.add(new Account("-1","Not available",0.0,regId));
        return list;
    }*/
}
