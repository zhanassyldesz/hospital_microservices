package microservices.demo.services;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import microservices.demo.modules.PatientRegisInfo;
import microservices.demo.modules.Regis;
import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

@Service
public class PatientRegistrationInformationService {

    @Autowired
    private RestTemplate restTemplate;

    @HystrixCommand(
            fallbackMethod = "getPatientRegisInfoFallback",
            threadPoolKey = "getPatientRegisInformation",
            threadPoolProperties = {
                    @HystrixProperty(name="coreSize", value="20"),
                    @HystrixProperty(name="maximumSize", value="10")
            })
    public PatientRegisInfo getPatientRegisInformation(String patientId){

        String apiCredentials = "rest-client:123";
        String base64Credentials = new String(Base64.encodeBase64(apiCredentials.getBytes()));

        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Basic " + base64Credentials);
        HttpEntity<String> entity = new HttpEntity<>(headers);

        return restTemplate.exchange("http://registration-info-service/regis/info/" + patientId,
                HttpMethod.GET, entity, PatientRegisInfo.class).getBody();
    }

    public PatientRegisInfo getPatientRegisInfoFallback(String patientId){

        PatientRegisInfo patientRegisInfo = new PatientRegisInfo();
        List<Regis> list = new ArrayList<>();
        list.add(new Regis("0","0","0","Not available","Not available","Not available","Not available"));
        patientRegisInfo.setPatientRegistration(list);

        return patientRegisInfo;
    }
}
